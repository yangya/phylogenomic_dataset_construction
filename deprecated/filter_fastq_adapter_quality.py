"""
Compare paired-end illumina reads with user-supplied adapter sequence
If adapters are detected in either reads, remove the entire read pair
Filter reads by quality scores before output .fq.filtered files

Dependancies: makeblastdb, blastn
"""

from seq_reader import fastq_generator
from numpy import mean
import sys,os

SAMPLE_FREQ = 100000
MIN_AVE_SCORE = 20.0
TRIM_TAIL_QUALS = 5 	#trim seq at the tail end with quality score lower than this number
KEEP_LEN = 30 			#only keep sequences longer than this after trimming ends

def run_cmd(cmd,logfile):
	print cmd
	with open(logfile,"a") as infile:
		infile.write(cmd+"\n")
	os.system(cmd)

def fastq_ok(fqfile):
	if not os.path.exists(fqfile): return False
	else:
		count = 0
		infile = open(fqfile,"r")
		for read_obj in fastq_generator(infile):
			count += 1
			if count == 10000: break
		infile.close()
		if count == 10000: return True
		else: return False

def blast_adapters(fq,adapter_file,num_cores,logfile):
	"""identify reads with adapter contamination"""
	fa = fq+".fasta"
	if os.path.exists(fq+".to_remove") and os.stat(fq+".to_remove").st_size > 0:
		return fq+".to_remove"
	#convert fastq to fasta
	if not os.path.exists(fa):
		cmd = "sed -n '1~4s/^@/>/p;2~4p' "+fq+" > "+fa
		run_cmd(cmd,logfile)
	#blast and pipe the output hits
	os.system("makeblastdb -in "+fa+" -dbtype nucl -out "+fa+".db")
	cmd = "blastn -query "+adapter_file
	cmd += " -db "+fa+".db -outfmt 6 -num_threads "+str(num_cores)+" -max_target_seqs 100000000 "
	cmd += "| awk '{print $2}' | sort | uniq >"+fq+".to_remove"
	run_cmd(cmd,logfile)
	if os.path.exists(fq+".to_remove") and os.stat(fq+".to_remove").st_size > 0:
		os.system("rm "+fa+".db* "+fa)
	else: sys.exit("Error in blastn")
	return fq+".to_remove"

def cut_fastq(seq_object,pointer):
	"output in four-line fastq format, cut at pointer"""
	fastq_str = "@"+seq_object.name+"\n"
	fastq_str += (seq_object.seq)[:pointer]+"\n"
	fastq_str += "+\n"
	fastq_str += (seq_object.qualstr)[:pointer]+"\n"
	return fastq_str

def check_phred_offset(quality_score_list):
	print "Quality score for the forward first read. Check phred offset"
	print "Change the offset in sequence.py if needed"
	print quality_score_list #quality array as a list
	for quality_score in quality_score_list:
		assert quality_score <= 41 and quality_score > 0, \
			"Change the offset in sequence.py"
	return

def log_stats(fq,adapter_file,count_original,count_adapter_removed,count_final,logfile):
	"""log summary stats for adapter and quality filtering"""
	with open(logfile,"a") as outfile:
		outfile.write("MIN_AVE_SCORE = "+str(MIN_AVE_SCORE)+"\n")
		outfile.write("TRIM_TAIL_QUALS = "+str(TRIM_TAIL_QUALS)+"\n")
		outfile.write("KEEP_LEN = "+str(KEEP_LEN)+"\n")
		outfile.write("adapter file: "+adapter_file+"\n")
		outfile.write("Original reads: "+str(count_original)+"\n")
		outfile.write("Reads after removing adapters: "+str(count_adapter_removed)+"\n")
		outfile.write("Final reads after thinning by quality: "+str(count_final)+"\n")
	print "Output written to .filtered files"
	print "Summary stats written to",logfile
	return
	
def filter_fastq_se(fq,adapter_file,num_cores,logfile):
	"""Filter paired end reads"""
	out = fq+".filtered"
	if os.path.exists(out) and os.stat(out).st_size > 0:
		print out,"exists"
		return out
	count_original,count_adapter_removed,count_final = 0,0,0
	#detect adapters
	to_remove = blast_adapters(fq,adapter_file,num_cores,logfile)
	with open(to_remove) as infile:
		adapterid_set = set(infile.read().splitlines())
	print len(adapterid_set),"reads will be removed"
	infile = open(fq,"r")
	outfile = open(out,"w")
	outfile_qual = open(fq+".qual","w") # quality scores for plotting in R
	for read_obj in fastq_generator(infile):
		count_original += 1 #keep track of number of input read pairs
		if count_original == 1: check_phred_offset(read_obj.qualarr)
		#shorten names here to match blast hits processed by awk
		name = (read_obj.name).split(" ")[0]
		if name in adapterid_set:
			#remove seqid to speed up remaining searches
			adapterid_set.remove(name)
		else: #does not contain adapter
			count_adapter_removed += 1
			if mean(read_obj.qualarr) > MIN_AVE_SCORE:
				pointer = len(read_obj.qualarr)
				while read_obj.qualarr[pointer-1] < TRIM_TAIL_QUALS:
					pointer -= 1
				if pointer > KEEP_LEN:
					outfile.write(cut_fastq(read_obj,pointer))
					count_final += 1			
		if count_original % SAMPLE_FREQ == 0:
			outfile_qual.write(" ".join([str(score) for score in read_obj.qualarr])+"\n")
			print "Read",count_original,"pairs,",count_adapter_removed,"after removing adapters,",count_final,"written to outfiles"
	outfile.close()
	outfile_qual.close()
	infile.close()
	log_stats(fq,adapter_file,count_original,count_adapter_removed,count_final,logfile)
	assert fastq_ok(out), "Error in filtering fastq files"
	return out

def filter_fastq_pe(fq1,fq2,adapter_file,num_cores,logfile):
	"""Filter paired end reads"""
	out1,out2 = fq1+".filtered",fq2+".filtered"
	if fastq_ok(out1) and fastq_ok(out2):
		print out1,out2,"exists"
		return out1,out2
	#detect adapters
	to_remove1 = blast_adapters(fq1,adapter_file,num_cores,logfile)
	to_remove2 = blast_adapters(fq2,adapter_file,num_cores,logfile)
	with open(to_remove1) as infile:
		adapterid_set = set(infile.read().splitlines())
	with open(to_remove2) as infile:
		adapterid_set = adapterid_set.union(set(infile.read().splitlines()))
	print len(adapterid_set),"read pairs will be removed"
	
	#filter reads by presence of adapter and quality scores
	print "Writing the processed fastq files"
	count_original,count_adapter_removed,count_final = 0,0,0
	infile1 = open(fq1,"r")
	infile2 = open(fq2,"r")
	outfile1 = open(out1,"w")
	outfile2 = open(out2,"w")
	outfile_qual = open(fq1+".qual","w") # quality scores for plotting in R
	read2_generator = fastq_generator(infile2)
	
	for read1_obj in fastq_generator(infile1):
		count_original += 1 #keep track of number of input read pairs
		if count_original == 1: check_phred_offset(read1_obj.qualarr)
		read2_obj = read2_generator.next()

		#Have to cut names here to match blast hits processed by awk
		name1 = (read1_obj.name).split(" ")[0]
		name2 = (read2_obj.name).split(" ")[0]
		#check to make sure that reads are paired
		assert name1 == name2, "reads are not perfectly paired: "+name1+" "+name2
		if name1 in adapterid_set:
			#remove seqid to speed up remaining searches
			adapterid_set.remove(name1)
		else: #does not contain adapter
			#add /1 and /2 to be compatible to Trinity
			read1_obj.name = name1+"/1"
			read2_obj.name = name2+"/2"
			count_adapter_removed += 1
			if mean(read1_obj.qualarr) > MIN_AVE_SCORE and mean(read2_obj.qualarr) > MIN_AVE_SCORE:
				pointer1 = len(read1_obj.qualarr)
				pointer2 = len(read2_obj.qualarr)
				while read1_obj.qualarr[pointer1-1] < TRIM_TAIL_QUALS:
					pointer1 -= 1
				while read2_obj.qualarr[pointer2-1] < TRIM_TAIL_QUALS:
					pointer2 -= 1
				if pointer1 > KEEP_LEN and pointer2 > KEEP_LEN:
					outfile1.write(cut_fastq(read1_obj,pointer1))
					outfile2.write(cut_fastq(read2_obj,pointer2))
					count_final += 1			
		if count_original % SAMPLE_FREQ == 0:
			outfile_qual.write(" ".join([str(score) for score in read1_obj.qualarr])+"\n")
			print "Read",count_original,"pairs,",count_adapter_removed,"after removing adapters,",count_final,"written to outfiles"
	outfile1.close()
	outfile2.close()
	outfile_qual.close()
	infile1.close()
	infile2.close()
	log_stats(fq1,adapter_file,count_original,count_adapter_removed,count_final,logfile)
	assert fastq_ok(out1) and fastq_ok(out2), "Error in filtering fastq files"
	return out1,out2


if __name__ == "__main__":
	if len(sys.argv) == 4:
		filter_fastq_se(fq=sys.argv[1],adapter_file=sys.argv[2],num_cores=int(sys.argv[3]))
	elif len(sys.argv) == 5:
		filter_fastq_pe(fq1=sys.argv[1],fq2=sys.argv[2],adapter_file=sys.argv[3],num_cores=int(sys.argv[4]))
	else:
		print "Usage"
		print "For single end reads: python filter_fastq.py fq_read adapter_file num_cores"
		print "For paired end reads: python filter_fastq.py fq_read1 fq_read2 adapter_file num_cores"
		sys.exit(0)
	



"""
Compare paired-end illumina reads with user-supplied adaptor sequence
If adapters are detected in reads, remove both reads in a read pair

Dependencies: NCBI blast+ package: make sure that makeblastdb and blastn are in the path

Input: thinned reads
Output: .thinned.cleaned remaining reads after removing adaptors
		adaptors.fastq containing the removed sequences
"""

from seq_reader import fastq_iterator
import sys,os

SAMPLE_FREQ = 100000
MIN_AVE_SCORE = 20.0
trim_tail_quals = 5 	#trim seq at the tail end with quality score lower than this number
keep_len = 30 			#only keep sequences longer than this after trimming ends

#identify reads with adaptor contamination
def blast_adaptors(fq,adaptor_file,num_cores):
	#convert fastq to fasta
	fa = fq+".fasta"
	cmd = "sed -n '1~4s/^@/>/p;2~4p' "+fq+" > "+fa
	print cmd
	os.system(cmd)
	#blast
	os.system("makeblastdb -in "+fa+" -dbtype nucl -out "+fq+".db")
	cmd = "blastn -query "+adaptor_file
	cmd += " -db "+fq+".db -outfmt 6 -num_threads "+str(num_cores)+" -max_target_seqs 100000000 "
	cmd += "| awk '{print $2}' | sort | uniq >"+fa+".to_remove"
	print cmd
	os.system(cmd)
	print "Removing the temporary blast database"
	os.system("rm "+fq+".db*")
	return

#output in two-line fasta format, cut at pointer
def cut_fasta(seq_object,pointer):
	fasta_str = ">"+seq_object.name+"\n"
	fasta_str += (seq_object.seq)[:pointer]+"\n"
	return fasta_str

#output in four-line fastq format, cut at pointer
def cut_fastq(seq_object,pointer):
	fastq_str = "@"+seq_object.name+"\n"
	fastq_str += (seq_object.seq)[:pointer]+"\n"
	fastq_str += "+\n"
	fastq_str += (seq_object.qualstr)[:pointer]+"\n"
	return fastq_str

if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "usage: python process_fastq_paired-end.py read1.fq read2.fq adapters num_cores"
		sys.exit(0)
	
	fq1 = sys.argv[1]
	fq2 = sys.argv[2]
	adaptor_file = sys.argv[3]
	num_cores = int(sys.argv[4])
	blast_adaptors(fq1,adaptor_file,num_cores)
	blast_adaptors(fq2,adaptor_file,num_cores)
	
	to_remove1 = []
	with open(fq1+".fasta.to_remove") as infile:
		for name in infile: to_remove1.append(name.strip())
	print "Adaptor detected in",len(to_remove1),"forward reads"
	set1 = set(to_remove1)
	to_remove2 = []
	with open(fq2+".fasta.to_remove") as infile:
		for name in infile: to_remove2.append(name.strip())
	print "Adaptor detected in",len(to_remove2),"reverse reads"
	set2 = set(to_remove2)

	print "Writing the processed fastq files"
	count_original = 0
	count_adapter_removed = 0
	count_final = 0 # after quality trimming
	infile1 = open(fq1,"r")
	infile2 = open(fq2,"r")
	outfile_errors = open(fq1+".adaptors","w")
	outfile1 = open(fq1+".filtered","w")
	outfile2 = open(fq2+".filtered","w")
	read2_iterator = fastq_iterator(infile2)
	
	for read1_obj in fastq_iterator(infile1):
		count_original += 1
		read2_obj = read2_iterator.next()
		#Have to cut names here to match blast hits processed by awk
		name1 = (read1_obj.name).split(" ")[0]
		name2 = (read2_obj.name).split(" ")[0]
		in1, in2 = False, False
		if name1 in set1: in1 = True
		if name2 in set2: in2 = True
		if in1 or in2:
			print "Removing",name1,name2
			outfile_errors.write(read1_obj.get_fastq()+read2_obj.get_fastq())
			if in1: set1.remove(name1)
			if in2: set2.remove(name2)
			#delete this seq name to speed up remaining searches
		else: # do not contain adapter
			#add /1 and /2 to be compatible to Trinity
			read1_obj.name = name1+"/1"
			read2_obj.name = name2+"/2"
			seq_quals1 = read1_object.qualarr #list of quality scores for read1
			seq_quals2 = read2_object.qualarr #list of quality scores for read2	
			count_adapter_removed += 1
			if count_adapter_removed == 1:
				print "Quality score for the forward first read. Check phred scale"
				print "Change the offset in sequence.py if needed"
				print seq_quals1
			if mean(seq_quals1) > MIN_AVE_SCORE and mean(seq_quals2) > MIN_AVE_SCORE:
				pointer1 = len(seq_quals1)
				pointer2 = len(seq_quals2)
				while seq_quals1[pointer1-1] < trim_tail_quals:
					pointer1 -= 1
				while seq_quals2[pointer2-1] < trim_tail_quals:
					pointer2 -= 1
				if pointer1 > keep_len and pointer2 > keep_len:
					outfile_fq1.write(cut_fastq(read1_object,pointer1))
					outfile_fq2.write(cut_fastq(read2_object,pointer2))
					count_final += 1			
					outfile1.write(read1_obj.get_fastq())
					outfile2.write(read2_obj.get_fastq())
		if count_original % SAMPLE_FREQ == 0:
			print "Read",count_original,"pairs,",count_adapter_removed,"after removing adapters", count_final,"written to outfiles"
	outfile1.close()
	outfile2.close()
	outfile_errors.close()
	infile1.close()
	infile2.close()

	#log summary stats
	with open(sys.argv[1]+".seq-process-log","a") as outfile:
		outfile.write("MIN_AVE_SCORE = "+str(MIN_AVE_SCORE)+"\n")
		outfile.write("trim_tail_quals = "+str(trim_tail_quals)+"\n")
		outfile.write("keep_len = "+str(keep_len)+"\n")
		outfile.write("Adaptor file: "+adaptor_file+"\n")
		outfile.write("Original read pairs: "+str(before)+"\n")
		outfile.write("Reads after removing adapters: "+str(count_adapter_removed)+"\n")
		outfile.write("Final reads after thinning by quality: "+str(count_final)+"\n")



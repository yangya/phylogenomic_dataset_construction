import sys,os

"""
this is wrapping the pruner and the seq extract
"""

treeending = ".mm"
seqending = ""

pp_cmd = "python ~/brlab_nextgen/paralogy_pruner.py"
pps_cmd = "python ~/brlab_nextgen/paralogy_pruner_seq_yy.py"

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python paralogy_pruner_wrapper_yy.py DIRtreefiles DIRorthofiles"
		sys.exit(0)
	DIR_treefiles = sys.argv[1]
	DIR_orthofiles = sys.argv[2]
	for i in os.listdir(DIR_treefiles):
		file_name = DIR_treefiles+"/"+i
		if file_name[-len(treeending):] == treeending:
#			cmd = pp_cmd+" "+file_name+" "+file_name+".pp"
#			print cmd
#			os.system(cmd)
			cmd = pps_cmd+" "+file_name+".pp "+DIR_orthofiles
			print cmd
			os.system(cmd)

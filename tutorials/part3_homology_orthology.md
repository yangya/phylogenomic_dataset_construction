#Part 3: homology and orthology
##Clustering
Reduce redundancy for cds (use -r 0 since these should all be positive strand after translation):
	
	cd ~/phylogenomic_dataset_construction/examples/homology_inference/data
	~/apps/TransDecoder/util/bin/cd-hit-est -i Beta.cds.fa -o Beta.fa.cds.cdhitest -c 0.99 -n 10 -r 0 -T 2
	~/apps/TransDecoder/util/bin/cd-hit-est -i HURS.cds.fa -o HURS.fa.cds.cdhitest -c 0.99 -n 10 -r 0 -T 2
	~/apps/TransDecoder/util/bin/cd-hit-est -i NXTS.cds.fa -o NXTS.fa.cds.cdhitest -c 0.99 -n 10 -r 0 -T 2
	~/apps/TransDecoder/util/bin/cd-hit-est -i RNBN.cds.fa -o RNBN.fa.cds.cdhitest -c 0.99 -n 10 -r 0 -T 2
	~/apps/TransDecoder/util/bin/cd-hit-est -i SCAO.cds.fa -o SCAO.fa.cds.cdhitest -c 0.99 -n 10 -r 0 -T 2

Concatenate all the .cdhitest files into a new file all.fa

	mkdir ../1_clustering
	cat *.cdhitest >../1_clustering/all.fa
	
All-by-all blast

	cd ../1_clustering
	makeblastdb -in all.fa -parse_seqids -dbtype nucl -out all.fa
	blastn -db all.fa -query all.fa -evalue 10 -num_threads 2 -max_target_seqs 30 -out all.rawblast -outfmt '6 qseqid qlen sseqid slen frames pident nident length mismatch gapopen qstart qend sstart send evalue bitscore'

This will take a few minutes. In the mean time you can open the unfinished output file all.rawblast to see what the results look like. The columns are as specified by the -outformat flag. Filter raw blast output by hit fraction and prepare input file for mcl. I usually use 0.3 or 0.4 for hit\_fraction_cutoff. A lower hit-fraction cutoff will output clusters with more incomplete sequences and larger and sparser alignments, whereas a high hit-fraction cutoff gives tighter clusters but ignores incomplete or divergent sequences.

	python ~/phylogenomic_dataset_construction/blast_to_mcl.py all.rawblast 0.4

As it is running, it outputs between taxa blast hits that are nearly identical. These can be from contamination, but can also be bacteria and fungal sequences. Double check when two samples have an unusually high number of identical sequences and this can be a sign of contamination. 

Run mcl. "--te" specifies number of threads, "-I" specifies the inflation value, and -tf 'gq()' specifies minimal -log transformed evalue to consider, and "-abc" specifies the input file format.

	mcl all.rawblast.hit-frac0.4.minusLogEvalue --abc -te 2 -tf 'gq(5)' -I 1.4 -o hit-frac0.4_I1.4_e5

Write fasta files for each cluster from mcl output that have all 5 taxa. Make a new directory to put the thousands of output fasta files.
	
	mkdir ../2_clusters
	python ~/phylogenomic_dataset_construction/write_fasta_files_from_mcl.py all.fa hit-frac0.4_I1.4_e5 5 ../2_clusters/

Now we have a new directory with fasta files that look like cluster1.fa, cluster2.fa and so on. For tutorial purpose let's only use 10% of the clusters
	
	cd ..
	mkdir 2_clusters_0
	cp 2_clusters/cluster*0.fa 2_clusters_0


##Build homolog trees

Align each cluster, trim alignment, and infer a tree. For cluster with less than 1000 sequences, it will be aligned with mafft (--genafpair --maxiterate 1000), trimmed by a minimal column occupancy of 0.1 and tree inference using raxml. For larger clusters it will be aligned with pasta, trimmed by a minimal column occupancy of 0.01 and tree inference using fasttree. The ouput tree files look like clusterID.raxml.tre or clusterID.fasttree.tre for clusters with 1000 or more sequences. 

	python ~/phylogenomic_dataset_construction/fasta_to_tree.py 2_clusters_0 2 dna n

This will take a few minutes to finish. In the mean time, you can visualize some of the trees and alignments. You can see that tips that are 0.4 or more are pretty much junk. There are also some tips that are much longer than near-by tips that are probably results of assembly artifacts.

Trim tips that are longer than 0.2 and more than 10 times longer than its sister. Also trim tips that are longer than 0.4.

	python ~/phylogenomic_dataset_construction/trim_tips.py 2_clusters_0 .tre 0.2 0.4

It outputs the tips that were trimmed off. The outputu are the .tt files. Mask both mono- and (optional) paraphyletic tips that belong to the same taxon. Keep the tip that has the most un-ambiguous charactors in the trimmed alignment. 

	python ~/phylogenomic_dataset_construction/mask_tips_by_taxonID_transcripts.py 2_clusters_0 2_clusters_0 y

The results are the .tt files. You can open up some of the larger tree files, such as cluster10.raxml.tre, cluster10.raxml.tre.tt, cluster10.raxml.tre.tt.mm and compare them. You may also notice that there are some long branches separating orthogroups. In this simple example these long branches are not particularly bad but with larger data set cutting long internal branches often significantly improve alignments.
	
	mkdir 3_homolog
	python ~/phylogenomic_dataset_construction/cut_long_internal_branches.py 2_clusters_0 .mm 0.3 5 3_homolog

The output are .subtree files in 3_homolog. Write fasta files from these .subtree files.

	python ~/phylogenomic_dataset_construction/write_fasta_files_from_trees.py 1_clustering/all.fa 3_homolog .subtree 3_homolog

Calculate the final homolog trees and bootstrap from these new fasta files

	python ~/phylogenomic_dataset_construction/fasta_to_tree.py 3_homolog 2 dna y
	
This takes about half an hour to run with 2 cores. In the mean time you can visualize the trees and alignments that are finished.
	
##Paralogy pruning to infer orthologs. Use one of the following:

1to1: only look at homologs that are strictly one-to-one. No cutting is carried out.
	
	cd ~/phylogenomic_dataset_construction/examples/homology_inference
	mkdir ortho_121
	mkdir ortho_121/tre
	python ~/phylogenomic_dataset_construction/filter_1to1_orthologs.py 3_homolog .tre 5 ortho_121/tre
	
The script will write one-to-one ortholog trees to the directory ortho_121/tre. You can play with the code to set a bootstrap filter etc.

MI: prune by maximum inclusion. The long_tip_cutoff here is typically the same as the value used when trimming tips. Set OUTPUT_1to1_ORTHOLOGS to False if wish only to ouput orthologs that is not 1-to-1, for example, when 1-to-1 orthologs have already been analyzed in previous steps.

	cd ~/phylogenomic_dataset_construction/examples/homology_inference
	mkdir ortho_MI
	mkdir ortho_MI/tre
	python ~/phylogenomic_dataset_construction/prune_paralogs_MI.py 3_homolog .tre 0.2 0.4 5 ortho_MI/tre

MO: prune by using homologs with monophyletic, non-repeating outgroups, reroot and cut paralog from root to tip. If no outgroup, only use those that do not have duplicated taxa. Change the list of ingroup and outgroup names first. Set OUTPUT_1to1_ORTHOLOGS to False if wish only to ouput orthologs that is not 1-to-1
	
	cd ~/phylogenomic_dataset_construction/examples/homology_inference
	mkdir ortho_MO
	mkdir ortho_MO/tre
	python ~/phylogenomic_dataset_construction/prune_paralogs_MO.py 3_homolog .tre 5 ortho_MO/tre

RT: prune by extracting ingroup clades and then cut paralogs from root to tip. If no outgroup, only use those that do not have duplicated taxa. Compile a list of ingroup and outgroup taxonID, with each line begin with either "IN" or "OUT", followed by a tab, and then the taxonID.

	cd ~/phylogenomic_dataset_construction/examples/homology_inference
	mkdir ortho_RT
	mkdir ortho_RT/tre
	python ~/phylogenomic_dataset_construction/prune_paralogs_RT.py 3_homolog .tre ortho_RT/tre 4 in_out

All the orthologs have full taxon occupancy (5 for 1-to-1, MI and MO; 4 for RT). 

Normally I write new fasta files from ortholog trees (write_ortholog_fasta_files.py) and use prank to estimate a new alignment for each ortholog (prank_wrapper.py). However, since we have a very small data set we can just extract aligned sequences from homolog alignments. Let's only do the 1-to-1 as an example
	
	mkdir ortho_121/aln
	python ~/phylogenomic_dataset_construction/write_alignments_from_orthologs.py 3_homolog/ ortho_121/tre ortho_121/aln

Trim alignment. I usually use 0.3 for MIN_COLUMN_OCCUPANCY

	python ~/phylogenomic_dataset_construction/phyutility_wrapper.py ortho_121/aln 0.3 dna

Choose the minimal cleaned alignment length (300 nucleotides) and minimal number of taxa filters for whether to include an ortholog in the supermatrix. Concatenate selected cleaned matrices:

	python ~/phylogenomic_dataset_construction/concatenate_matrices.py ortho_121/aln 300 5 dna 121_filter300-5

This will output a summary of taxon matrix occupancies to check whether any taxon is under represented, a concatenated matrix in phylip format and the .model file for raxml

##Estimate species tree

Run raxml with each ortholog as a separate partition.

	raxml -T 2 -p 12345 -m GTRCAT -q 121_filter300-5.model -s 121_filter300-5.phy -n 121_filter300-5

Run 200 jackknife replicates resampling 10% genes each time
	
	cd ~/phylogenomic_dataset_construction/examples/homology_inference/
	mkdir JK10
	cp 121_filter300-5.model 121_filter300-5.phy JK10
	cd JK10
	python ~/phylogenomic_dataset_construction/jackknife_by_percent_genes.py 2 0.1 dna
	cat *result* >../JK10_trees

Alternatively, resampling 5 genes for each jackknife replication:
	
	cd ~/phylogenomic_dataset_construction/examples/homology_inference/
	python ~/phylogenomic_dataset_construction/jackknife_by_number_genes.py . 2 5 dna
	cat 5genes/*result* >5genes_trees

Mapping jackknife results to the best tree.
	
	cd ~/phylogenomic_dataset_construction/examples/homology_inference/
	raxml -f b -t RAxML_bestTree.121_filter300-5 -z JK10_trees -T 2 -m GTRCAT -n 121_filter300-5_JK10
	raxml -f b -t RAxML_bestTree.121_filter300-5 -z 5genes_trees -T 2 -m GTRCAT -n 121_filter300-5_5genes

You can open the files RAxML_bipartitions.121_filter300-5_5genes and RAxML_bipartitions.121_filter300-5_JK10 to visualize the jackknife support values

##Explore homolog tree discordance using phyparts
Extract rooted ingroup clades with all four taxa present from homologs and write them to the directory inclades. Also write the multi-labeled ingroup clades to the directory inclades_phyparts.

	cd ~/phylogenomic_dataset_construction/examples/homology_inference/
	mkdir inclades
	mkdir inclades_phyparts
	python ~/phylogenomic_dataset_construction/extract_clades.py 3_homolog .tre inclades 4 in_out inclades_phyparts/
	
Conflict and concordance

	phyparts -a 1 -d inclades_phyparts -m RAxML_bestTree.121_filter300-5 -o concon -s 50 -v
	
Duplications

	phyparts -a 2 -d inclades_phyparts -m RAxML_bestTree.121_filter300-5 -o dup -s 50 -v


	
	

